const webpackconfig = require('react-scripts/config/webpack.config');
const config = webpackconfig('development');

const ExtensionReloader = require('webpack-extension-reloader')
const CopyWebpackPlugin = require('copy-webpack-plugin')


// Soft eject
module.exports = Object.assign({}, config, {
  plugins: [
    ...config.plugins,

    // hotheloader module
    new ExtensionReloader({
      port: 9090, // Which port use to create the server
      reloadPage: true, // Force the reload of the page also
    }),

    // expose public path
    new CopyWebpackPlugin({
      patterns: [
        { from: 'public' },
      ]
    })
  ],
})
